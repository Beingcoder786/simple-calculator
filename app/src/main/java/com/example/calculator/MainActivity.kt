package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    var op="+"
    var oldnumber=""
    var isnewop=true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

      var cl:Button=findViewById(R.id.cl)
        var editText:EditText=findViewById(R.id.editText)
        cl.setOnClickListener {
            editText.setText("")
        }
    }
      fun numberEvent(view: View)
      {
          var editText:EditText=findViewById(R.id.editText)

          if(isnewop)
              editText.setText("")

          isnewop=false

          var button1:Button=findViewById(R.id.button1)

          var button2:Button=findViewById(R.id.button2)

          var button3:Button=findViewById(R.id.button3)

          var button4:Button=findViewById(R.id.button4)


          var button5:Button=findViewById(R.id.button5)
          var button6:Button=findViewById(R.id.button6)
          var button7:Button=findViewById(R.id.button7)
          var button8:Button=findViewById(R.id.button8)
          var button9:Button=findViewById(R.id.button9)
          var button0:Button=findViewById(R.id.button0)
          var buttondot:Button=findViewById(R.id.buttondot)

          var buclick=editText.text.toString()
          var buselect:Button=view as Button
          when(buselect.id)
          {
             button1.id->buclick+="1"
             button2.id->buclick+="2"
              button3.id->buclick+="3"
              button4.id->buclick+="4"
              button5.id->buclick+="5"
              button6.id->buclick+="6"
              button7.id->buclick+="7"
              button8.id->buclick+="8"
              button9.id->buclick+="9"
              button0.id->buclick += "0"
              buttondot.id->buclick+="."

          }
          editText.setText(buclick.toString())

      }
    fun operEvent(view: View)
    {
        isnewop=true
        var add:Button=findViewById(R.id.add)
        var minus:Button=findViewById(R.id.minus)
        var multiplay:Button=findViewById(R.id.multiplay)
        var divive:Button=findViewById(R.id.divive)

        var editText:EditText=findViewById(R.id.editText)
        oldnumber=editText.text.toString()

        var buselect=view as Button
        when(buselect.id)
        {
         add.id->{ op="+"}
         minus.id->{ op="-" }
         multiplay.id->{ op="*"}
         divive.id->{op="/" }
        }
    }
    fun equalEvent(view: View)
    {
        var editText:EditText=findViewById(R.id.editText)

        var newnumber=editText.text.toString()
        var result=0.0
        when(op)
        {
            "+"->{result=oldnumber.toDouble()+newnumber.toDouble()}
            "-"->{result=oldnumber.toDouble()-newnumber.toDouble()}
            "*"->{result=oldnumber.toDouble()*newnumber.toDouble()}
            "/"->{result=oldnumber.toDouble()/newnumber.toDouble()}

        }
        editText.setText(result.toString())
    }

}